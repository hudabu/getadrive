# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
   parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
   },
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list

## Stack :

**React** (TypeScript) build with **Vite**.

## Dependencies used :

### 💄 Styling :

#### Modern CSS

> [**TAILWINDCSS**](https://tailwindcss.com/docs/installation)
>
> Tailwind CSS works by scanning all of your HTML files, JavaScript components, and any other templates for class names, generating the corresponding styles and then writing them to a static CSS file.

#### Component library

> [**SHADCN**](https://ui.shadcn.com/)
>
> Beautifully designed components that you can copy and paste into your apps. Accessible. Customizable. Open Source.

### 🎉 Data mocking :

#### Data moking

> [**FAKERJS**](https://fakerjs.dev/)
>
> Generate massive amounts of fake (but realistic) data for testing and development.

#### API moking

> [**MIRAGEJS**](https://miragejs.com/)
>
> Mirage JS is an API mocking library that lets you build, test and share a complete working JavaScript application without having to rely on any backend services.

### 🧰 Utilities

#### Fetching

> [**TENSTACK QUERY**](https://tanstack.com/query/latest/docs/react/quick-start)
>
> Headless, type-safe, & powerful utilities for State Management, Routing, Data Visualization, Charts, Tables, and more.
