import { fetchAllPeople } from "@/fetchers";
import { Profile } from "./Profile";
import { useQuery } from "@tanstack/react-query";

export function People() {
  // Fetch data from the API
  const { isPending, data, error } = useQuery({
    queryKey: ["people"],
    queryFn: () => fetchAllPeople(),
  });

  if (isPending) return <span>Loading...</span>;

  if (error) return <span>Error: {error.message}</span>;

  // We can assume that `isSuccess === true`
  return (
    <div>
      <header className="prose m-6">
        <h1>
          <span role="img" aria-label="sparkles">
            ✨
          </span>
          People
          <span role="img" aria-label="sparkles">
            ✨
          </span>
        </h1>
      </header>

      <section className="flex flex-wrap">
        {data.people.map((person) => (
          <Profile key={`person-${person.id}`} {...person} />
        ))}
      </section>
    </div>
  );
}
