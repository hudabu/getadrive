import { createServer, Model, Factory } from "miragejs";
import { fakerFR as faker } from "@faker-js/faker";
import { Person } from "./fetchers";

export function makeServer({ environment = "test" }) {
  return createServer({
    environment,

    factories: {
      person: Factory.extend<Partial<Person>>({
        get firstName() {
          return faker.person.firstName();
        },
        get lastName() {
          return faker.person.lastName();
        },
        get name() {
          return this.firstName + " " + this.lastName;
        },
        get streetAddress() {
          return faker.location.streetAddress();
        },
        get phone() {
          return faker.phone.number();
        },
        get username() {
          return faker.internet.userName(this.firstName, this.lastName);
        },
        get password() {
          return faker.internet.password();
        },
        get email() {
          return faker.internet.email(
            this.firstName,
            this.lastName,
            "example.com"
          );
        },
        get avatar() {
          return faker.internet.avatar();
        },
      }),
    },

    models: {
      person: Model.extend<Partial<Person>>({}),
    },

    routes() {
      this.namespace = "api";

      this.get("people");
    },

    seeds(server) {
      server.createList("person", 20);
    },
  });
}
