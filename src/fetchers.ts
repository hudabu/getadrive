import axios from "axios";

export type Person = {
  id: string;
  firstName: string;
  lastName: string;
  name: string;
  streetAddress: string;
  phone: string;
  username: string;
  password: string;
  email: string;
  avatar: string;
};

type PeopleResponse = {
  people: Person[];
};

export const fetchAllPeople = () =>
  axios.get("api/people").then<PeopleResponse>((res) => res.data);
